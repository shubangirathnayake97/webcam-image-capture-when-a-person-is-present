import cv2 
import time

# load the cascade
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# laod capture video from webcam 
cap = cv2.VideoCapture(0) # 0 for default cam

count = 0 
while True:
    # Read the frame 
    _, img = cap.read()

    # convert to grayscale 
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # dETECT THE FACES 
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    # draw the rectangle arounf the face 
    for(x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x+w, y+h), (255,0,0), 2)

        # display 
        cv2.imshow('img', img)
        t = time.strftime("%Y-%m-%d_%H-%M-%S")

        print("Image" +t +"saved")
        file = 'D:\Projects\Webcam Image Capture When A Person is Present/'+t+'.jpg'
        cv2.imwrite(file,img)
        count +=1

    # stop if ESC is pressed 
    k = cv2.waitKey(30) & 0xff
    if k==27:
        break

#release the videoCapture object
cap.release()